
# matrix-tencent-forward-bot

A forward bot between Matrix and Tencent QQ.

# Dependency

- libkazv
- OneBot Kotlin
- libhttpserver

# Build and Use

## For Gentoo users

If you are using Gentoo, you can use [tusooa-overlay][tusooa-overlay]
to install matrix-tencent-forward-bot.

[tusooa-overlay]: https://gitlab.com/tusooa/tusooa-overlay

# Usage

0. Register a bot account on a matrix server.

1. Run `mtfb` to obtain an access token for the bot.

2. Configure OneBot Kotlin and mirai as required.

3. Create a configuration file:

```json
{
"matrixDeviceId":"<device id obtained in Step 1>",
"matrixId":"<fully qualified user id of the bot, e.g. @bot:example.org>",
"matrixOwnerId":"<fully qualified user id of bot owner, this user will be invited to each matrix room that has set up a mapping, when that room is created; e.g. @human:example.org>",
"matrixRoomPrefix":"<room alias prefix -- when creating a mapping room, a room alias will be set and it is prefixed by the value of this configuration; e.g. foo- ; this means the mapping room alias would be #foo-123456:example.org, etc.>",
"matrixServer":"<url for your homeserver, e.g. https://example.org>",
"matrixToken":"<token obtained in Step 1>",
"oneBotPostPort":<customizable port number for where to run the post server to receive messages from OneBot Kotlin, e.g. 1234>,
"oneBotServer":"<OneBot Kotlin http server address, e.g. http://0.0.0.0:5700/>",
"oneBotToken":"<the token for OneBot Kotlin>",
"hideOwnerSenderName":<bool, if true, hides the sender name of the owner when forwarding from matrix to tencent>
}
```

4. Run `mtfb path/to/config/file <context>` to set up a forwarding between that context and a newly
   created matrix room. The bot owner (`matrixOwnerId`) will be automatically invited to that room.
   The matrix room is set to be private.
   The configuration file will be automatically updated.

   A context is:

   - the group number, if you would like to forward a Tencent group.
   - the account number, plus the letter `P`, if you would like to forward a Tencent friend.
     For example, if a friend's account number is `123456`, the context is `123456P`.

5. Repeat 4 until you have done setting up forwarding.

6. Run `mtfb path/to/config/file` to start forwarding.

## Docker

Assume we are storing the relevant data for mtfb (config files) in the directory
`/path/to/data` on host machine.

Note: if you run mtfb in docker, you will probably want to run mirai in another docker
container as well, and put them on the same network, in order for the two to comminucate
properly.

### Auto-generation

You can generate the config file automatically by using

```
docker run -e VAR=val -e VAR2=val2 ... -v /path/to/data:/data reg.lily.kazv.moe/kazv/matrix-tencent-forward-bot:servant-prod -g
```

For a list of the environment variables needed, and their explanation, run

```
docker run reg.lily.kazv.moe/kazv/matrix-tencent-forward-bot:servant-prod -h
```

After generating you can use the following to start mtfb (the environment variables are only needed for generation):

```
docker run -d -v /path/to/data:/data reg.lily.kazv.moe/kazv/matrix-tencent-forward-bot:servant-prod
```

### Set up forwarding

Use:

```
docker run -d -v /path/to/data:/data reg.lily.kazv.moe/kazv/matrix-tencent-forward-bot:servant-prod -a CONTEXT
```

The definition of CONTEXT is given above.

# Acknowledgement

This project uses [mirai][mirai] and [OneBot Kotlin][obk] for the Tencent QQ
protocol. The matrix protocol is based on [libkazv][libkazv].

[mirai]: https://github.com/mamoe/mirai
[obk]: https://github.com/yyuueexxiinngg/onebot-kotlin
[libkazv]: https://gitlab.com/kazv/libkazv

# License

Copyright (C) 2020 Tusooa Zhu

matrix-tencent-forward-bot is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

matrix-tencent-forward-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with matrix-tencent-forward-bot.  If not, see <https://www.gnu.org/licenses/>.
