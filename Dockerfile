ARG LIBKAZV_TAG
FROM reg.lily.kazv.moe/kazv/libkazv/libkazv:$LIBKAZV_TAG

RUN apt-get update && \
    DPKG_FRONTEND=noninteractive apt-get -y upgrade && \
    DPKG_FRONTEND=noninteractive apt-get -y install \
    libboost-regex-dev
ARG BUILD_TYPE=Debug
ARG JOBS=3

ARG DEPS_INSTALL_DIR=/opt/libkazv-deps
ARG LIBKAZV_INSTALL_DIR=/opt/libkazv
ARG MTFB_INSTALL_DIR=/opt/mtfb

COPY deploy/docker-entry /

RUN mkdir -pv /build
COPY . /build/libkazv

WORKDIR /build/libkazv

RUN mkdir build && cd build && \
    cmake .. -DCMAKE_INSTALL_PREFIX="$MTFB_INSTALL_DIR" -DCMAKE_PREFIX_PATH="$LIBKAZV_INSTALL_DIR;$DEPS_INSTALL_DIR" -DCMAKE_BUILD_TYPE="$BUILD_TYPE" && \
    make -j$JOBS && \
    ./src/mtfbtest && \
    make -j$JOBS install && \
    cd ../.. && rm -rf /build

ENTRYPOINT ["/docker-entry"]
