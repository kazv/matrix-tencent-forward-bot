/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of matrix-tencent-forward-bot.
 *
 * matrix-tencent-forward-bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * matrix-tencent-forward-bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with matrix-tencent-forward-bot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SendMessageJob.hpp"

#include <iostream>

#include <cursorutil.hpp>

namespace OneBot
{

    static Kazv::JsonBody buildBody(std::optional<std::int_fast64_t> userId,
                                    std::optional<std::int_fast64_t> groupId,
                                    Kazv::JsonWrap message)
    {
        nlohmann::json j;

        if (userId) { j["user_id"] = userId.value(); }
        if (groupId) { j["group_id"] = groupId.value(); }

        j["message"] = message.get();

        std::cout << "Request body will be: " << j.dump() << std::endl;

        return j;
    }

    SendMessageJob::SendMessageJob(std::string serverUrl,
                                   std::string _accessToken,
                                   std::optional<std::int_fast64_t> userId,
                                   std::optional<std::int_fast64_t> groupId,
                                   Kazv::JsonWrap message)
        : Kazv::BaseJob(serverUrl,
                        "/send_msg",
                        Kazv::BaseJob::POST,
                        "SendMessage",
                        _accessToken,
                        Kazv::BaseJob::ReturnType::Json,
                        buildBody(userId, groupId, message))
    {
    }

    SendMessageJob::JobResponse::JobResponse(Kazv::Response r)
        : Kazv::Response(std::move(r))
    {
    }

    bool SendMessageResponse::success() const
    {
        return Kazv::Response::success()
            && isBodyJson(body)
            && jsonBody().get().contains("status")
            && (jsonBody().get()["status"].get<std::string>() == "ok");
    }
}
