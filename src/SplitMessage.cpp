/*
 * Copyright (C) 2021 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of matrix-tencent-forward-bot.
 *
 * matrix-tencent-forward-bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * matrix-tencent-forward-bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with matrix-tencent-forward-bot.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "SplitMessage.hpp"
#include <boost/regex.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <immer/flex_vector_transient.hpp>
#include <lager/util.hpp>
#include <iostream>

namespace OneBot
{
    namespace {
        auto tokenizer = boost::regex(
            R"XXX((?<text>[^\[]+)|(?<cq-code>\[CQ:(?<cq-code-content>[^\]]+)\]))XXX",
            boost::regex::perl | boost::regex::no_mod_m);

        auto imageRegex = boost::regex(R"XXX(image,file=(?<image-fn>[^,]+),url=(?<image-url>.+))XXX");

        auto cqCodeTokenizer = boost::regex(
            R"XXX((?<argName>[^,=]+)=(?<argContent>[^,]+)|(?<func>[^,=]+))XXX");

        std::string unescapeStr(std::string escaped) {
            using namespace std::string_literals;
            boost::algorithm::replace_all(escaped, "&#91;"s, "["s);
            boost::algorithm::replace_all(escaped, "&#93;"s, "]"s);
            boost::algorithm::replace_all(escaped, "&#44;"s, ","s);
            boost::algorithm::replace_all(escaped, "&amp;"s, "&"s);
            return escaped;
        }

        template<class RangeT>
        struct ConditionalBackInserter
        {
            ConditionalBackInserter(RangeT &container) : container(container) {}

            template<class T>
            ConditionalBackInserter &operator=(std::optional<T> maybeElement) {
                if (maybeElement) {
                    container.insert(maybeElement.value());
                }
                return *this;
            }

            ConditionalBackInserter &operator*() { return *this; }

            ConditionalBackInserter &operator++() { return *this; }
            ConditionalBackInserter &operator++(int) { return *this; }

            RangeT &container;
        };

        template<class RangeT, class FuncT>
        struct MaybeMergeBackInserter
            : public std::iterator<std::output_iterator_tag,
                                   void, void, void, void>
        {
            using ValT = MaybeMergeBackInserter;
            MaybeMergeBackInserter(RangeT &container, FuncT tryMergeFunction)
                : container(container)
                , tryMerge(tryMergeFunction) {}

            template<class T>
            ValT &operator=(T elem) {
                if (container.empty()) {
                    container.push_back(std::move(elem));
                    return *this;
                }
                auto res = tryMerge(container.at(container.size() - 1), elem);
                if (res) {
                    container.set(container.size() - 1, *res);
                } else {
                    container.push_back(std::move(elem));
                }
                return *this;
            }

            ValT &operator*() { return *this; }

            ValT &operator++() { return *this; }
            ValT &operator++(int) { return *this; }

            RangeT &container;
            FuncT tryMerge;
        };
    }

    CQCode::CQCode()
    {
    }

    CQCode::CQCode(std::string funcName, std::unordered_map<std::string, std::string> args)
        : funcName(funcName)
        , args(args)
    {
    }

    CQCode::CQCode(std::string content)
    {
        auto first = boost::sregex_iterator(content.begin(), content.end(), cqCodeTokenizer);

        if (! (*first)["func"].matched) {
            // Bad cq code
            return;
        }

        funcName = unescapeStr(std::string((*first)["func"].first, (*first)["func"].second));

        // skip function name
        first++;
        auto construct =
            [](auto match) -> std::optional<std::pair<std::string, std::string>> {
                if (! match["argName"].matched) {
                    return std::nullopt;
                }
                auto argName = unescapeStr(std::string(match["argName"].first, match["argName"].second));
                auto argContent = unescapeStr(std::string(match["argContent"].first, match["argContent"].second));
                return std::pair(argName, argContent);
            };

        std::transform(
            first,
            boost::sregex_iterator(),
            ConditionalBackInserter(args),
            construct);
    }

    Message splitMessage(std::string raw)
    {
        auto msg = Message{}.transient();

        auto convertCQCode =
            [=](auto cqcode, auto raw) -> Segment {
                if (cqcode.funcName == "image") {
                    return Image{cqcode.args["file"], cqcode.args["url"]};
                } else if (cqcode.funcName == "share") {
                    return Share{cqcode.args["url"], cqcode.args["title"],
                            cqcode.args["content"], cqcode.args["image"]};
                } else if (cqcode.funcName == "at") {
                    return At{cqcode.args["qq"]};
                }

                return Text{"[CQ:" + raw + "]"};
            };

        auto construct =
            [=](auto match) -> Segment {
                if (match["text"].matched) {
                    return Text{unescapeStr(std::string(match["text"].first, match["text"].second))};
                } else {
                    // assert(match["cq-code"].matched);
                    auto content = std::string(match["cq-code-content"].first, match["cq-code-content"].second);
                    return convertCQCode(CQCode(content), content);
                }
            };

        std::transform(
            boost::sregex_iterator(raw.begin(), raw.end(), tokenizer),
            boost::sregex_iterator(),
            std::back_inserter(msg),
            construct);

        return msg.persistent();
    }

    Segment tryStringifySegment(Segment s)
    {
        return lager::match(std::move(s))(
            [](Share s) -> Segment {
                return Text{"[" + (s.title.size() ? s.title : s.url) + "](" + s.url + ")"};
            },
            [](Image s) -> Segment {
                return s;
            },
            [](Text s) -> Segment {
                return s;
            },
            [](At s) -> Segment {
                return Text{"@" + s.tencent};
            }
            );
    }

    Message compactMessage(Message orig)
    {
        auto tmp = immer::flex_vector_transient<Segment>{};

        std::transform(orig.begin(), orig.end(),
                       std::back_inserter(tmp), tryStringifySegment);

        auto tryMerge =
            [](Segment a, Segment b) -> std::optional<Segment>
            {
                if (std::holds_alternative<Text>(a)
                    && std::holds_alternative<Text>(b)) {
                    return Text{std::get<Text>(a).text + std::get<Text>(b).text};
                } else {
                    return std::nullopt;
                }
            };

        auto ret = immer::flex_vector_transient<Segment>{};
        std::copy(tmp.begin(),
                  tmp.end(),
                  MaybeMergeBackInserter(ret, tryMerge));
        return ret.persistent();
    }
}
