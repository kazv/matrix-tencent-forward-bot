/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of matrix-tencent-forward-bot.
 *
 * matrix-tencent-forward-bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * matrix-tencent-forward-bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with matrix-tencent-forward-bot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string>
#include <tuple>

#include <nlohmann/json.hpp>

enum ContextType
{
    Private,
    Group
};

struct Context : public std::pair<ContextType, std::string>
{
    using BaseT = std::pair<ContextType, std::string>;
    explicit Context(const nlohmann::json &msg);
    explicit Context(std::string contextId);

    std::string toString() const;
};
