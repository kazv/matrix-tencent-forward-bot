/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of matrix-tencent-forward-bot.
 *
 * matrix-tencent-forward-bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * matrix-tencent-forward-bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with matrix-tencent-forward-bot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <httpserver.hpp>

#include <functional>

using namespace httpserver;

using Handler = std::function<std::shared_ptr<http_response>(const http_request &)>;

class Resource : public http_resource
{
public:
    using FuncT = Handler;
    inline Resource(FuncT f) : f(std::move(f)) {}

    inline const std::shared_ptr<http_response> render(const http_request &req) {
        return f(req);
    }
private:
    FuncT f;
};

inline void runServer(int port, Handler h)
{
    webserver ws = create_webserver(port);

    Resource r{h};
    ws.register_resource("/", &r);
    ws.start(true);
}
