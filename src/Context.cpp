/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of matrix-tencent-forward-bot.
 *
 * matrix-tencent-forward-bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * matrix-tencent-forward-bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with matrix-tencent-forward-bot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstdint>

#include "Context.hpp"

static bool isGroupMsg(const nlohmann::json &msg)
{
    return msg.at("message_type").get<std::string>() == "group";
}

static bool isGroupMsg(std::string ctxId)
{
    return ctxId.back() != 'P';
}

Context::Context(const nlohmann::json &msg)
    : BaseT{isGroupMsg(msg) ? Group : Private,
            isGroupMsg(msg) ? std::to_string(msg.at("group_id").get<std::int_fast64_t>())
            : std::to_string(msg.at("user_id").get<std::int_fast64_t>())}
{
}

Context::Context(std::string ctxId)
    : BaseT{isGroupMsg(ctxId) ? Group : Private,
            isGroupMsg(ctxId) ? ctxId
            : std::string(ctxId.cbegin(), ctxId.cend() - 1)}
{
}

std::string Context::toString() const
{
    auto [type, id] = *this;
    if (type == Group) {
        return id;
    }
    return id + "P";
}
