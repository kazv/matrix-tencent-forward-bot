/*
 * Copyright (C) 2021 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of matrix-tencent-forward-bot.
 *
 * matrix-tencent-forward-bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * matrix-tencent-forward-bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with matrix-tencent-forward-bot.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <base/basejob.hpp>

namespace OneBot
{
    class GetImageJob : public Kazv::BaseJob
    {
    public:
        explicit GetImageJob(std::string serverUrl,
                             std::string _accessToken,
                             std::string filename
            );

        class JobResponse : public Kazv::Response
        {
        public:
            JobResponse(Kazv::Response r);
            bool success() const;
            std::string localFileName() const;
            std::string contentType() const;
        };
    };

    using GetImageResponse = GetImageJob::JobResponse;
}
