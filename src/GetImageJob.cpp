/*
 * Copyright (C) 2021 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of matrix-tencent-forward-bot.
 *
 * matrix-tencent-forward-bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * matrix-tencent-forward-bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with matrix-tencent-forward-bot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "GetImageJob.hpp"

namespace OneBot
{
    static Kazv::JsonBody buildBody(std::string filename)
    {
        return nlohmann::json::object({{"file", filename}});
    }

    GetImageJob::GetImageJob(std::string serverUrl,
                             std::string _accessToken,
                             std::string filename)
        : Kazv::BaseJob(serverUrl,
                        "/get_image",
                        Kazv::BaseJob::POST,
                        "GetImage",
                        _accessToken,
                        Kazv::BaseJob::ReturnType::Json,
                        buildBody(filename))
    {
    }

    GetImageJob::JobResponse::JobResponse(Kazv::Response r)
        : Kazv::Response(std::move(r))
    {
    }

    bool GetImageResponse::success() const
    {
        return Kazv::Response::success()
            && isBodyJson(body)
            && jsonBody().get().contains("status")
            && (jsonBody().get()["status"].get<std::string>() == "ok");
    }

    std::string GetImageResponse::localFileName() const
    {
        return jsonBody().get().at("data").at("file").template get<std::string>();
    }

    std::string GetImageResponse::contentType() const
    {
        auto type = jsonBody().get().at("data").at("file_type").template get<std::string>();
        if (type == "jpg") {
            return "image/jpeg";
        } else if (type == "gif") {
            return "image/gif";
        } else if (type == "png") {
            return "image/png";
        }
        return "image/x-unknown";
    }
}
