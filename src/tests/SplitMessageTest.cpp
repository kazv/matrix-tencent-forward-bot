/*
 * Copyright (C) 2021 Tusooa Zhu
 *
 * This file is part of libkazv.
 *
 * libkazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * libkazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with libkazv.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <libkazv-config.hpp>

#include <catch2/catch.hpp>

#include <iostream>

#include "SplitMessage.hpp"

TEST_CASE("Should parse CQ Code properly", "[splitmessage]")
{
    using namespace OneBot;
    REQUIRE(CQCode("image,file=xxx") == CQCode{"image", {{"file", "xxx"}}});
    REQUIRE(CQCode("fake") == CQCode{"fake", {}});

    REQUIRE(CQCode("image,file=xxx,url=yyy") == CQCode{"image", {{"file", "xxx"},
                                                                 {"url", "yyy"}}});

    REQUIRE(CQCode("fake,tis=&#91;&#44;&#93;") == CQCode{"fake", {{"tis", "[,]"}}});
    REQUIRE(CQCode("fake,tis=&#91;&amp;&#44;&#93;") == CQCode{"fake", {{"tis", "[&,]"}}});
    REQUIRE(CQCode("fake,tis=&#91;&amp;#44;&#93;") == CQCode{"fake", {{"tis", "[&#44;]"}}});

    // invalid ones, should ignore it
    REQUIRE(CQCode("foo=bar") == CQCode{});
    REQUIRE(CQCode("foo,bar=xxx,mew") == CQCode{"foo", {{"bar", "xxx"}}});
}

TEST_CASE("Split message should work properly", "[splitmessage]")
{
    std::string raw = "xxx[CQ:image,file=foobar,url=http://example.xxx/foobar]yyy[CQ:image,file=foobar,url=http://example.xxx/foobar][CQ:image,file=foobar,url=http://example.xxx/foobar]";

    auto msg = OneBot::splitMessage(raw);

    for (auto seg : msg) {
        if (std::holds_alternative<OneBot::Text>(seg)) {
            std::cout << "text: " << std::get<OneBot::Text>(seg).text << std::endl;
        } else {
            std::cout << "image: " << std::get<OneBot::Image>(seg).filename
                      << "," << std::get<OneBot::Image>(seg).url << std::endl;
        }
    }

    REQUIRE(msg == decltype(msg){
            OneBot::Text{"xxx"},
            OneBot::Image{"foobar", "http://example.xxx/foobar"},
            OneBot::Text{"yyy"},
            OneBot::Image{"foobar", "http://example.xxx/foobar"},
            OneBot::Image{"foobar", "http://example.xxx/foobar"},
        });
}

TEST_CASE("Should compact message properly", "[splitmessage]")
{
    using namespace OneBot;
    REQUIRE(compactMessage(Message{}) == Message{});
    REQUIRE(compactMessage(Message{Text{"foo"}}) == Message{Text{"foo"}});
    REQUIRE(compactMessage(Message{Text{"foo"},
                                   Text{"bar"}})
        == Message{Text{"foobar"}});

    REQUIRE(compactMessage(Message{Text{"foo"},
                                   Image{"xxx", "http://yyy.xxx/zzz"},
                                   Text{"bar"}})
        == Message{Text{"foo"},
                   Image{"xxx", "http://yyy.xxx/zzz"},
                   Text{"bar"}});

    REQUIRE(compactMessage(Message{Text{"foo"},
                                   Share{"http://yyy.xxx/zzz", "title", "", ""},
                                   At{"233"},
                                   Text{"bar"}})
        == Message{Text{"foo[title](http://yyy.xxx/zzz)@233bar"}});
}
