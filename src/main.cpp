/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of matrix-tencent-forward-bot.
 *
 * matrix-tencent-forward-bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * matrix-tencent-forward-bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with matrix-tencent-forward-bot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <sstream>
#include <fstream>
#include <chrono>

#include <boost/asio.hpp>

#include <lager/store.hpp>
#include <lager/event_loop/boost_asio.hpp>

#include <asio-promise-handler.hpp>

#include <job/cprjobhandler.hpp>
#include <eventemitter/lagerstoreeventemitter.hpp>

#include <client/sdk.hpp>

#include "PostServer.hpp"
#include "Context.hpp"
#include "SendMessageJob.hpp"
#include "GetImageJob.hpp"
#include "SplitMessage.hpp"

using namespace Kazv::CursorOp;

int main(int argc, char *argv[])
{
    boost::asio::io_context ioContext;
    auto eventEmitter =
        Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{ioContext.get_executor()});

    Kazv::CprJobHandler jobHandler(ioContext.get_executor());

    auto sdk = Kazv::makeSdk(
        Kazv::SdkModel{},
        static_cast<Kazv::JobInterface &>(jobHandler),
        static_cast<Kazv::EventInterface &>(eventEmitter),
        Kazv::AsioPromiseHandler{ioContext.get_executor()},
        zug::identity
        );

    auto ctx = sdk.context();
    auto sdkContext = sdk.context();
    auto client = sdk.client();

    auto watchable = eventEmitter.watchable();

    nlohmann::json cfgJson;

    auto generating = false;

    if (argc <= 1) {
        generating = true;
        std::cout << "No config file specified. Please enter your login details:" << std::endl;
        std::string homeserver;
        std::string username;
        std::string password;
        std::cout << "Homeserver: (e.g. https://example.org)" << std::endl;
        std::getline(std::cin, homeserver);
        std::cout << "Username: (e.g. example)" << std::endl;
        std::getline(std::cin, username);
        std::cout << "Password:" << std::endl;
        std::getline(std::cin, password);

        client.passwordLogin(homeserver, username, password, "mtfb/libkazv")
            .then([=](auto res) {
                if (!res.success()) {
                    std::cerr << "Login failed." << std::endl;
                    std::exit(1);
                }
                using namespace Kazv::CursorOp;

                std::cout << "Your access token is: " << +client.token() << std::endl;

                std::cout << "Your device id is: " << +client.deviceId() << std::endl;

                std::exit(0);

            });
    } else {
        std::string configFN{argv[1]};
        std::cout << "Using config file: " << configFN << std::endl;
        {
            std::ifstream file(configFN);
            if (!file) {
                std::cout << "Cannot open that file. Stop." << std::endl;
                std::exit(0);
            }

            std::ostringstream ss;
            ss << file.rdbuf();
            std::string str = ss.str();

            cfgJson = nlohmann::json::parse(str);
        }
        std::string homeserver = cfgJson.at("matrixServer");
        std::string userId = cfgJson.at("matrixId");
        std::string token = cfgJson.at("matrixToken");
        std::string deviceId = cfgJson.at("matrixDeviceId");

        std::cout << "Your user id is: " << userId << std::endl;

        client.tokenLogin(homeserver, userId, token, deviceId);

        if (argc >= 3) {
            std::string tencent{argv[2]};
            auto [type, id] = Context(tencent);
            std::cout << "You are to add mapping for [Tencent] " << tencent << std::endl;

            watchable.after<Kazv::CreateRoomSuccessful>(
            [=](auto &&e) {
                using namespace Kazv::CursorOp;

                auto roomId = e.roomId;

                std::cout << "The created room id is: " << roomId << std::endl;

                auto newJson = cfgJson;

                if (!newJson.contains("roomMapping")) {
                    newJson["roomMapping"] = nlohmann::json::object();
                }
                newJson["roomMapping"][tencent] = roomId;

                std::cout << "The updated json file is:" << std::endl;
                std::cout << newJson.dump() << std::endl;

                std::cout << "Updating your config file..." << std::endl;
                {
                    std::ofstream file(configFN);
                    file << newJson.dump();
                }
                std::cout << "Done." << std::endl;

                std::exit(0);
            });
            watchable.after<Kazv::CreateRoomFailed>(
                [=](auto &&e) {
                    std::cout << "Create room failed" << std::endl;
                    std::cout << e.errorCode << std::endl;
                    std::cout << e.error << std::endl;
                    std::exit(0);
                });

            Kazv::RoomVisibility v = Kazv::Private;
            std::string owner = cfgJson.at("matrixOwnerId").get<std::string>();
            immer::array<std::string> invite{owner};

            auto alias = cfgJson.at("matrixRoomPrefix").get<std::string>() + tencent;

            std::cout << "The room alias name (local part) would be: " << alias << std::endl;

            std::cout << "The bot owner (" << owner
                      << ") will also be invited to this room." << std::endl;

            Kazv::json powerLevels = Kazv::json::object();
            powerLevels["users"] = Kazv::json::object();
            powerLevels["users"][userId] = 100;
            powerLevels["users"][owner] = 50;

            client.createRoom(v, "[Tencent] " + tencent,
                              std::nullopt, //alias,
                              invite,
                              std::nullopt, //isDirect,
                              true, // allowFederate,
                              std::nullopt, //topic
                              powerLevels);
        }
    }

    if (generating) {
        std::thread([&] { ioContext.run(); }).join();
        std::exit(0);
    }

    auto oneBotServer = cfgJson.at("oneBotServer").get<std::string>();
    auto oneBotToken = cfgJson.at("oneBotToken").get<std::string>();

    std::thread([&] { ioContext.run(); }).detach();

    using Int64 = std::int_fast64_t;

    std::map<Context, std::string> tencentToMatrix;
    std::map<std::string, Context> matrixToTencent;

    if (cfgJson.contains("roomMapping")) {
        auto mapping = cfgJson["roomMapping"];
        std::cout << "Loading room mapping..." << std::endl;
        for (auto& [tencent, matrix] : mapping.items()) {
            std::cout << "[Tencent] " << tencent << " <=> " << "[Matrix] " << matrix << std::endl;
            Context ctx(tencent);
            tencentToMatrix.insert_or_assign(ctx, matrix);
            matrixToTencent.insert_or_assign(matrix, ctx);
        }
        std::cout << "Room mapping loaded." << std::endl;
    }

    watchable.after<Kazv::LoginSuccessful>(
        [](auto &&) {

        });

    bool afterFirstSync = false;

    watchable.after<Kazv::SyncSuccessful>(
        [&](auto &&) {
            afterFirstSync = true;
        });

    watchable.after<Kazv::SendMessageSuccessful>(
        [](auto &&e) {
            std::cout << "Successfully sent " << e.eventId
                      << " to " << e.roomId << " ." << std::endl;
        });

    watchable.after<Kazv::SendMessageFailed>(
        [](auto &&e) {
            std::cout << "Failed sending "
                      << " to " << e.roomId << " ." << std::endl;
            std::cout << e.errorCode << " " << e.error << std::endl;
        });

    auto shouldForwardMatrix =
        [&](std::string roomId) {
            return matrixToTencent.find(roomId) != matrixToTencent.end();
        };

    bool hideOwnerSenderName = cfgJson.contains("hideOwnerSenderName")
        ? cfgJson["hideOwnerSenderName"].get<bool>()
        : true;

    std::string owner = cfgJson.at("matrixOwnerId").get<std::string>();

    watchable.after<Kazv::ReceivingRoomTimelineEvent>(
        [&](auto &&e) {
            using namespace Kazv::CursorOp;

            if (! afterFirstSync) {
                std::cout << "This is a historical message. Discarding." << std::endl;
                return;
            }

            auto event = e.event;
            std::cout << "Getting: " << event.originalJson().get().dump() << std::endl;
            if (event.sender() == +client.userId()) {
                std::cout << "This is a self-sent event. Ignoring it." << std::endl;
                return;
            }

            if (event.type() != "m.room.message"
                || ! shouldForwardMatrix(e.roomId)) {
                std::cout << "should not forward this" << std::endl;
                return;
            }

            std::cout << "should forward this" << std::endl;

            auto content = event.content().get();

            std::string msgtype;

            if (content.contains("msgtype")) {
                msgtype = content["msgtype"];
            }

            auto createTextSegment =
                [](std::string text) {
                    return nlohmann::json::object({
                            {"type", "text"},
                            {"data", nlohmann::json::object({
                                        {"text", text}
                                    })}
                        });
                };

            auto getBody =
                [=]() -> std::optional<nlohmann::json> {
                    std::string body = content.contains("body") ? content["body"] : "";
                    std::string url = content.contains("url") ? content["url"] : "";
                    if (msgtype == "m.text" || msgtype == "m.emote") {
                        return createTextSegment(body);
                    } else if (msgtype == "m.image") {
                        return nlohmann::json::object({
                                {"type", "image"},
                                {"data", {{"file", client.mxcUriToHttp(url)}}},
                            });
                    } else if (msgtype == "m.file") {
                        return createTextSegment("File(" + body + "): " + client.mxcUriToHttp(url));
                    } else if (msgtype == "m.audio") {
                        return nlohmann::json::object({
                                {"type", "record"},
                                {"data", {{"file", client.mxcUriToHttp(url)}}}
                            });
                    } else if (msgtype == "m.video") {
                        return nlohmann::json::object({
                                {"type", "video"},
                                {"data", {{"file", client.mxcUriToHttp(url)}}},
                            });
                    }
                    return std::nullopt;
                };

            auto bodyOpt = getBody();
            if (! bodyOpt) {
                std::cout << "The msgtype is not supported yet." << std::endl;
                return;
            }
            auto body = bodyOpt.value();

            std::cout << "content body is: " << body.dump() << std::endl;

            auto senderId = event.sender();

            auto room = client.room(e.roomId);

            auto memEvent = +room.memberEventFor(senderId);

            auto senderName = senderId;

            /*if (memEvent) {
                auto content = memEvent.value().content().get();
                if (content.contains("displayname")) {
                    senderName = content.at("displayname");
                }
                }*/

            auto [ctxType, id] = Context{matrixToTencent.at(e.roomId)};

            auto shouldShowSenderName =
                (ctxType == Group
                 && ! (hideOwnerSenderName && owner == senderId));
            auto j = nlohmann::json::array();
            if (shouldShowSenderName) {
                j.push_back(createTextSegment("<" + senderName + ">: "));
            }
            j.push_back(body);

            std::optional<Int64> userId;
            if (ctxType == Private) { userId = std::stoll(id); }
            std::optional<Int64> groupId;
            if (ctxType == Group) { groupId = std::stoll(id); }

            OneBot::SendMessageJob job(
                oneBotServer,
                oneBotToken,
                userId,
                groupId,
                j);

            jobHandler.submit(
                std::move(job),
                [](Kazv::Response r) {
                    OneBot::SendMessageResponse res{r};
                    if (res.success()) {
                        std::cout << "Message sent successfully." << std::endl;
                    } else {
                        std::cout << "Failed sending message." << std::endl;
                        std::cout << "Status code: " << res.statusCode << std::endl;
                        if (std::holds_alternative<Kazv::BytesBody>(res.body)) {
                            std::cout << "Response body: "
                                      << std::get<Kazv::BytesBody>(res.body) << std::endl;
                        } else {
                            std::cout << "Response body: " << res.jsonBody().get().dump() << std::endl;
                        }
                    }
                });
        });

    auto shouldForwardTencent =
        [&](Context ctx) {
            return tencentToMatrix.find(ctx) != tencentToMatrix.end();
        };

    auto forwardTencentMsgIfNeeded =
        [=, &jobHandler](const nlohmann::json &msg) {
            std::cout << "Getting: " << msg.dump() << std::endl;
            Context ctx(msg);
            std::cout << "Context is: " << ctx.toString() << std::endl;

            if (shouldForwardTencent(ctx)) {
                std::cout << "should forward this" << std::endl;
                auto mxRoomId = tencentToMatrix.at(ctx);

                auto content = msg.at("raw_message").template get<std::string>();

                auto splitedMsg = OneBot::compactMessage(OneBot::splitMessage(content));

                const auto &sender = msg.at("sender");

                auto senderName = sender.contains("nickname")
                    ? sender.at("nickname").template get<std::string>()
                    : msg.at("user_id").template get<std::string>();

                auto sendTextMsgWithSenderName =
                    [=](std::string msg) {
                        auto event = nlohmann::json::object({
                                {"type", "m.room.message"},
                                {"content", {
                                        {"msgtype", "m.text"},
                                        {"format", "org.matrix.custom.html"},
                                        {"formatted_body", "<span data-mx-color='#33ccff'>" + senderName + "</span>: " + msg},
                                        {"body", senderName + ": " + msg},
                                    }},
                            });
                        client.room(mxRoomId).sendMessage(event);
                    };

                auto processSegment =
                    [=, &jobHandler](auto segment) {
                        lager::match(segment)(
                            [=](OneBot::Text s) {
                                sendTextMsgWithSenderName(s.text);
                            },
                            [](OneBot::At) { /* should not happen */ },
                            [](OneBot::Share) { /* should not happen */ },
                            [=, &jobHandler](OneBot::Image s) {
                                OneBot::GetImageJob job(
                                    oneBotServer,
                                    oneBotToken,
                                    s.filename);

                                jobHandler.submit(
                                    std::move(job),
                                    [=](Kazv::Response r) {
                                        OneBot::GetImageResponse res{r};
                                        if (! res.success()) {
                                            std::cout << "Cannot get image." << std::endl;
                                            return;
                                        }
                                        Kazv::FileDesc desc{res.localFileName(), res.contentType()};
                                        client.uploadContent(desc)
                                            .then([=](auto status) {
                                                      if (! status.success()) {
                                                          std::cout << "Cannot upload file to matrix server." << std::endl;
                                                          return sdkContext.createResolvedPromise(status);
                                                      }

                                                      auto mxcUri = status.dataStr("mxcUri");
                                                      auto event = nlohmann::json::object({
                                                              {"type", "m.room.message"},
                                                              {"content", {
                                                                      {"msgtype", "m.image"},
                                                                      {"url", mxcUri},
                                                                      {"body", "Image by " + senderName},
                                                                  }},
                                                          });

                                                      return client.room(mxRoomId).sendMessage(event);
                                                  })
                                            .then([=](auto status) {
                                                      if (! status.success()) {
                                                          std::cout << "Cannot send image msg to matrix server." << std::endl;
                                                          return;
                                                      }

                                                      sendTextMsgWithSenderName("<image> ^^^");
                                                  });
                                    });
                            }
                            );
                    };

                for (auto seg : splitedMsg) {
                    processSegment(seg);
                }
            } else {
                std::cout << "should not forward this" << std::endl;
            }
        };

    auto handlePost =
        [=](const httpserver::http_request &req) {
            auto msg = nlohmann::json::parse(req.get_content());

            auto handler =
                [=, msg=std::move(msg)](auto &&) {
                    try {
                        if (msg.at("post_type").get<std::string>() == "message") {
                            forwardTencentMsgIfNeeded(msg);
                        }
                    } catch (const std::exception &) {
                    }
                };
            // Make it run in the event loop thread
            sdkContext.createResolvedPromise({})
                .then(handler);

            return std::make_shared<httpserver::http_response>(string_response("", 204));
        };

    int port = cfgJson.at("oneBotPostPort");
    runServer(port, handlePost);
}
