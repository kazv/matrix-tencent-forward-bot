/*
 * Copyright (C) 2021 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of matrix-tencent-forward-bot.
 *
 * matrix-tencent-forward-bot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * matrix-tencent-forward-bot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with matrix-tencent-forward-bot.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <string>
#include <variant>
#include <immer/flex_vector.hpp>

// [CQ:image,file=3ABE8AE8620B0D4B9FBF175933DF07D0,url=http://gchat.qpic.cn/gchatpic_new/2334862046/574623640-2740191048-3ABE8AE8620B0D4B9FBF175933DF07D0/0?term=2]

namespace OneBot
{
    struct CQCode
    {
        CQCode();
        explicit CQCode(std::string content);
        CQCode(std::string funcName, std::unordered_map<std::string, std::string> args);

        std::string funcName;
        std::unordered_map<std::string, std::string> args;
    };

    inline bool operator==(const CQCode &a, const CQCode &b)
    {
        return a.funcName == b.funcName
            && a.args == b.args;
    }

    struct Text
    {
        std::string text;
    };

    inline bool operator==(Text a, Text b)
    {
        return a.text == b.text;
    }

    struct Image
    {
        std::string filename;
        std::string url;
    };

    inline bool operator==(Image a, Image b)
    {
        return a.filename == b.filename
            && a.url == b.url;
    }

    struct Share
    {
        std::string url;
        std::string title;
        std::string content;
        std::string image;
    };

    inline bool operator==(Share a, Share b)
    {
        return a.url == b.url
            && a.title == b.title
            && a.content == b.content
            && a.image == b.image;
    }

    struct At
    {
        std::string tencent;
    };

    inline bool operator==(At a, At b)
    {
        return a.tencent == b.tencent;
    }

    using Segment = std::variant<Text, Image, Share, At>;

    Segment tryStringifySegment(Segment s);

    inline bool operator!=(Segment a, Segment b)
    {
        return !(a == b);
    }
    using Message = immer::flex_vector<Segment>;

    Message splitMessage(std::string raw);

    Message compactMessage(Message orig);
}
