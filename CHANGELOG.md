
# Unreleased

- Support forwarding images directly to Matrix. https://gitlab.com/kazv/matrix-tencent-forward-bot/-/commit/a91c3a3c6a20034634db64ec97782e6e36cf5c77
- Support forwarding images directly from Matrix. https://gitlab.com/kazv/matrix-tencent-forward-bot/-/commit/68e5274f07886e93035adedaca495d762a69ba38
- Colourize nicks forwarded to Matrix. https://gitlab.com/kazv/matrix-tencent-forward-bot/-/commit/4b03a3349d8dc7179988f3d4af68277a6bc7ac68
